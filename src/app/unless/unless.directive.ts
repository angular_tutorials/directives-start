import { Directive, Input, ViewContainerRef, TemplateRef } from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {
  @Input() set appUnless(condition: boolean) {
    if (!condition) {
      this.vRef.createEmbeddedView(this.templateRef);
    } else {
      this.vRef.clear();
    }
  }
  constructor(private templateRef: TemplateRef<any>, private vRef: ViewContainerRef) { }

}
